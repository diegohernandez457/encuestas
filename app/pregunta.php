<?php


require_once 'conexion.php';

$db = new Conexion();
$con = $db->conectar();

if (isset($_POST["id"])) {
    
    $encuesta = $_POST["encuesta"];
    $indicador = $_POST["indicador"];
    $pregunta = $_POST["pregunta"];
    $tipo = $_POST["tipo"];
    $id = $_POST["id"];

    if($tipo == 1){

        $query = $con->prepare("UPDATE preguntas SET encuesta_id=:encuesta, indicador=:indicador, pregunta=:pregunta, tipo=:tipo WHERE id=:id");

        $query->execute(array(
            'encuesta' => $encuesta,
            'indicador' => $indicador,
            'pregunta' => $pregunta,
            'tipo' => $tipo,
            'id' => $id
        ));

    }else{

        $query = $con->prepare("UPDATE preguntas SET encuesta_id=:encuesta, indicador=:indicador, pregunta=:pregunta, tipo=:tipo WHERE id=:id");

        $query->execute(array(
            'encuesta' => $encuesta,
            'indicador' => $indicador,
            'pregunta' => $pregunta,
            'tipo' => $tipo,
            'id' => $id
        ));

    }

    header("Location: http://localhost/encuestas/views/crear_encuesta.php?id=".$encuesta);
    die();


}else{

    $encuesta = $_POST["encuesta"];
    $indicador = $_POST["indicador"];
    $pregunta = $_POST["pregunta"];
    $tipo = $_POST["tipo"];

    if ($tipo == 1) {
        
        $query = $con->prepare("INSERT INTO preguntas (encuesta_id, indicador, pregunta, tipo) VALUES (:encuesta, :indicador, :pregunta, :tipo)");

        $query->execute(array(
            'encuesta' => $encuesta,
            'indicador' => $indicador,
            'pregunta' => $pregunta,
            'tipo' => $tipo
        ));

    }else{

        $query = $con->prepare("INSERT INTO preguntas (encuesta_id, indicador, pregunta, tipo) VALUES (:encuesta, :indicador, :pregunta, :tipo)");

        $query->execute(array(
            'encuesta' => $encuesta,
            'indicador' => $indicador,
            'pregunta' => $pregunta,
            'tipo' => $tipo
        ));

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Se inserta los items, del combo.

        $pregunta_id = $con->lastInsertId();
        $items = $_POST["items"];

        $cadena = "INSERT INTO items (pregunta_id, item) VALUES ";

        for ($i=0; $i < count($items); $i++) { 
            $cadena.="('".$pregunta_id."', '".$items[$i]."'),";
        }

        $cadena_final = substr($cadena, 0, -1);
        $cadena_final.=";";

        echo $cadena_final;

        $query1 = $con->prepare($cadena_final);
        $query1->execute();

    }

    header("Location: http://localhost/encuestas/views/crear_encuesta.php?id=".$encuesta);
    die();

}


?>