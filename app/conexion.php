<?php

    class Conexion{

        private $server = 'localhost';
        private $database = 'encuesta';
        private $username = 'root';
        private $password = '';

        function conectar(){

            try {
                $pdo = new PDO("sqlsrv:Server=$this->server;Database=$this->database", NULL, NULL); //conexion con SQLServer
                return $pdo;
            } catch (PDOException $pe) {
                die("PDOException: " . $pe->getMessage());
            }
        }
    }

    //$pdo = new PDO("mysql:host=$this->server;dbname=$this->database", $this->username, $this->password); //conexion con Mysql

?>