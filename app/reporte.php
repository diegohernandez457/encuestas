<?php

    ob_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.min.css">
    <title>Document</title>
    <style>
        body{
            font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .card-header{
            margin-bottom: 40px;
            color: #31C5EA;
        }
        p{
            margin-left: 20px;
        }
        label{
            font-weight: bold;
        }
    </style>
</head>

<body>

    <?php

        require_once 'conexion.php';

        $db = new Conexion();
        $con = $db->conectar();

        $sql = $con->query("SELECT preguntas.indicador, preguntas.pregunta, respuestas.respuesta FROM respuestas INNER JOIN preguntas ON respuestas.pregunta_id = preguntas.id WHERE respuestas.encuesta_id = ".$_GET['id']);
        $sql->execute();
        $respuestas = $sql->fetchAll(PDO::FETCH_ASSOC);

    ?>

    <div class="container mt-5">
        <div class="row mt-5">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h1>Respuestas</h1>
                        <hr>
                    </div>
                    <div class="card-body">
                        <?php foreach($respuestas AS $row){ ?>
                            <div class="m-3">
                                <div class="mb-3 fw-bold">
                                    <label><?php echo $row['indicador'] ?>)</label>
                                    <label><?php echo $row['pregunta'] ?></label>
                                </div>
                                <p><?php echo $row['respuesta'] ?></p>
                            </div>
                            
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

</body>

</html>

<?php

    $html = ob_get_clean();

    require_once '../vendor/dompdf/autoload.inc.php';

    use Dompdf\Dompdf;
    $dompdf = new Dompdf();

    $dompdf->loadHtml($html);
    $dompdf->setPaper('Carta', 'Arial');
    $dompdf->render();
    $dompdf->stream("Encuesta.pdf", ['Attachment' => false]);
    exit(0);

?>