$(document).ready(function() {

  $("#divItem").hide();

  $("select#tipo").change(function() {
    var valor = $(this).val();

    if (valor == 2) {
      $("#divItem").show();
    }else{
      $("#divItem").hide();
    }
  });

  $("#btnAddItem").click(function() {

    var item = $("#txtItem").val();

    $('#items').append($('<option>', { 
      value: item,
      text : item
    }));

    $("#txtItem").val('');
  
  });

  ///////////////////////////////////////

  $("#divItemEditar").hide();

  $("select#tipoEditar").change(function() {
    var valor = $(this).val();

    if (valor == 2) {
      $("#divItemEditar").show();
    }else{
      $("#divItemEditar").hide();
    }
  });

  $("#btnAddItemEditar").click(function() {

    var item = $("#txtItemEditar").val();

    $('#itemsEditar').append($('<option>', { 
      value: item,
      text : item
    }));

    $("#txtItemEditar").val('');
  
  });


    
});

function editar(param) {

  $.ajax({
    type: "GET",
    url: "http://localhost/encuestas/app/editar_pregunta.php?id="+param,
    dataType: "json",
    success: function (data){

        $("#idEditar").val(data[0]['id']);
        $("#indicadorEditar").val(data[0]['indicador']);
        $("#preguntaEditar").val(data[0]['pregunta']);
        if (data[0]['tipo'] == 1) {
          $("#tipoEditar option[value='1']").attr("selected", true);
        }else if(data[0]['tipo'] == 2){
          $("#tipoEditar option[value='2']").attr("selected", true);
        }

        $("#modalEditarPregunta").modal("show");
    
    }
  });
  
}


