<?php

require_once '../app/conexion.php';

$db = new Conexion();
$con = $db->conectar();

$sql = $con->query("SELECT * FROM preguntas WHERE encuesta_id = ".$_GET['id']);
$sql->execute();
$preguntas = $sql->fetchAll(PDO::FETCH_ASSOC);

//////////////////////////////////////////////

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.min.css">
    <title>Document</title>
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <div class="card bg-light">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-2">
                            <li class="breadcrumb-item"><a href="http://localhost/encuestas/">Encuestas</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Diseñar</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Diseñar encuesta
                        <button type="button" class="btn btn-primary btn-sm float-end" data-bs-toggle="modal" data-bs-target="#modalCrearPregunta">Crear Pregunta</button>
                    </div>
                    <div class="card-body">
                        <?php foreach($preguntas AS $row){  ?>
                            <div class="row m-2">
                                <div class="mt-3 mb-2 fw-bold">
                                    <label><?php echo $row['indicador'] ?>)</label>
                                    <label><?php echo $row['pregunta'] ?></label>
                                </div>
                                <?php if($row['tipo'] == 1){ ?>
                                    <textarea name="" id="" cols="30" rows="3" class="form-control"></textarea>
                                <?php } else{ ?>
                                    <select class="form-select" aria-label="Default select example">
                                        <?php
                                            $sql = $con->query("SELECT * FROM items WHERE pregunta_id = ".$row['id']);
                                            $sql->execute();
                                            $items = $sql->fetchAll(PDO::FETCH_ASSOC);
                                        ?>
                                        <?php foreach($items AS $item){ ?>
                                            <option value="<?php echo $item['item'] ?>"><?php echo $item['item'] ?></option>
                                        <?php } ?>
                                    </select>
                                <?php } ?>
                            </div>
                            <a href="javascript:editar(<?php echo $row['id'] ?>)" class="m-2 text-decoration-none">Editar</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Modal Agregar Pregunta-->
    <div class="modal fade" id="modalCrearPregunta" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Crear Pregunta</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="../app/pregunta.php">
                        <input type="hidden" value="<?php echo $_GET['id'] ?>" name="encuesta">
                        <div class="mb-3">
                            <label for="indicador" class="form-label">Indicador:</label>
                            <input type="text" class="form-control" id="indicador" name="indicador">
                        </div>
                        <div class="mb-3">
                            <label for="pregunta" class="form-label">Pregunta:</label>
                            <input type="text" class="form-control" id="pregunta" name="pregunta">
                        </div>
                        <div class="mb-3">
                            <label for="tipo" class="form-label">Tipo de pregunta:</label>
                            <select class="form-select" id="tipo" name="tipo">
                                <option value="1">Texto</option>
                                <option value="2">Combo</option>
                            </select>
                        </div>
                        <div class="mb-3" id="divItem">
                            <label for="items" class="form-label">Items:</label>
                            <div class="mb-2">
                                <div class="row">
                                    <div class="col-8">
                                      <input type="text" name="item" id="txtItem" class="form-control">
                                    </div>
                                    <div class="col">
                                        <button class="btn btn-primary btn-md float-end" id="btnAddItem" type="button">Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <select class="form-select" id="items" name="items[]" multiple>
                            </select>
                        </div>                 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Editar Pregunta -->

    <div class="modal fade" id="modalEditarPregunta" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editar Pregunta</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="../app/pregunta.php">
                        <input type="hidden" name="id" id="idEditar">
                        <input type="hidden" value="<?php echo $_GET['id'] ?>" name="encuesta">
                        <div class="mb-3">
                            <label for="indicador" class="form-label">Indicador:</label>
                            <input type="text" class="form-control" id="indicadorEditar" name="indicador">
                        </div>
                        <div class="mb-3">
                            <label for="pregunta" class="form-label">Pregunta:</label>
                            <input type="text" class="form-control" id="preguntaEditar" name="pregunta">
                        </div>
                        <div class="mb-3">
                            <label for="tipo" class="form-label">Tipo de pregunta:</label>
                            <select class="form-select" id="tipoEditar" name="tipo">
                                <option value="1">Texto</option>
                                <option value="2">Combo</option>
                            </select>
                        </div>
                        <div class="mb-3" id="divItemEditar">
                            <label for="items" class="form-label">Items:</label>
                            <div class="mb-2">
                                <div class="row">
                                    <div class="col-8">
                                      <input type="text" name="item" id="txtItemEditar" class="form-control">
                                    </div>
                                    <div class="col">
                                        <button class="btn btn-primary btn-md float-end" id="btnAddItemEditar" type="button">Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <select class="form-select" id="itemsEditar" name="items[]" multiple>
                            </select>
                        </div>                 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    <input type="submit" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>


</body>

<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../assets/scripts.js"></script>

</html>