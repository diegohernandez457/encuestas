<?php

require_once '../app/conexion.php';

$db = new Conexion();
$con = $db->conectar();

$sql = $con->query("SELECT * FROM preguntas WHERE encuesta_id = ".$_GET['id']);
$sql->execute();
$preguntas = $sql->fetchAll(PDO::FETCH_ASSOC);

//////////////////////////////////////////////

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.min.css">
    <title>Document</title>
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <div class="card bg-light">
                   <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-2">
                            <li class="breadcrumb-item"><a href="http://localhost/encuestas/">Encuestas</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Responder</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Responder encuesta
                    </div>
                    <div class="card-body">
                       <form action="../app/respuesta.php" method="POST">
                            <input type="hidden" value="<?php echo $_GET['id'] ?>" name="idEncuesta">
                            <?php $i=0; foreach($preguntas AS $row){ ?>
                                <input type="hidden" name="encuesta[]" value="<?php echo $_GET['id'] ?>">
                                <input type="hidden" name="pregunta[]" value="<?php echo $row['id'] ?>">
                                <div class="m-3">
                                   <div class="mb-3">
                                        <label><?php echo $row['indicador'] ?>)</label>
                                        <label><?php echo $row['pregunta'] ?></label>
                                    </div>
                                    <?php if($row['tipo'] == 1){ ?>
                                        <textarea name="respuesta[]" cols="30" rows="3" class="form-control"></textarea>
                                    <?php } else{ ?>
                                        <select class="form-select" aria-label="Default select example" name="respuesta[]">
                                        <?php
                                            $sql = $con->query("SELECT * FROM items WHERE pregunta_id = ".$row['id']);
                                            $sql->execute();
                                            $items = $sql->fetchAll(PDO::FETCH_ASSOC);
                                        ?>
                                        <?php foreach($items AS $item){ ?>
                                            <option value="<?php echo $item['item'] ?>"><?php echo $item['item'] ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php } ?>
                                </div>
                            <?php $i++; } ?>
                            <input type="hidden" name="iterador" value="<?php echo $i ?>">
                            <input type="submit" class="btn btn-primary m-3">
                       </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
</body>

<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../assets/scripts.js"></script>

</html>