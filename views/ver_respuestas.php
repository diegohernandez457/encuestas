<?php

require_once '../app/conexion.php';

$db = new Conexion();
$con = $db->conectar();

$sql = $con->query("SELECT preguntas.indicador, preguntas.pregunta, respuestas.respuesta FROM respuestas INNER JOIN preguntas ON respuestas.pregunta_id = preguntas.id WHERE respuestas.encuesta_id = ".$_GET['id']);
$sql->execute();
$preguntas = $sql->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.min.css">
    <title>Document</title>
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col">
                <div class="card bg-light">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-2">
                        <li class="breadcrumb-item"><a href="http://localhost/encuestas/">Encuestas</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Respestas</li>
                    </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        Respuestas
                        <a href="http://localhost/encuestas/app/reporte.php?id=<?php echo $_GET['id']; ?>" target="_blank" class="btn btn-danger btn-sm float-end">Imprimir</a>
                    </div>
                    <div class="card-body">
                        <?php foreach($preguntas AS $row){ ?>
                            <div class="m-3">
                                <div class="mb-3 fw-bold">
                                    <label><?php echo $row['indicador'] ?>)</label>
                                    <label><?php echo $row['pregunta'] ?></label>
                                </div>
                                <p><?php echo $row['respuesta'] ?></p>
                            </div>
                            
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

</body>

<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../assets/scripts.js"></script>

</html>