<?php
require_once 'app/conexion.php';
//comentario

$db = new Conexion();
$con = $db->conectar();

$sql = $con->query("SELECT * FROM encuestas");

$sql->execute();

$resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <title>Encuestas</title>
</head>

<body>
    <main>
        <div class="container mt-5">
            <div class="row">
                <div class="col">
                    <div class="card bg-light">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb m-2">
                            <li class="breadcrumb-item active" aria-current="page">Home</li>
                        </ol>
                    </nav>
                    </div>
                </div>
            </div>

            <div class="row mt-5">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            Encuestas
                            <button type="button" class="btn btn-primary btn-sm float-end" data-bs-toggle="modal" data-bs-target="#exampleModal">Nueva</button>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 20%;">#</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($resultado as $row) {
                                ?>
                                    <tr>
                                        <td><?php echo $row['id']; ?></td>
                                        <td><?php echo $row['nombre']; ?></td>
                                        <td>
                                            
                                            <?php if ($row['estado'] == 0) { ?>
                                                <a href="http://localhost/encuestas/views/crear_encuesta.php?id=<?php echo $row['id'] ?>" class="btn btn-success btn-sm">Diseñar</a>
                                                <a href="http://localhost/encuestas/views/responder_encuesta.php?id=<?php echo $row['id'] ?>" class="btn btn-secondary btn-sm">Responder</a>
                                            <?php }else{ ?>
                                                <a href="http://localhost/encuestas/views/ver_respuestas.php?id=<?php echo $row['id'] ?>" class="btn btn-primary btn-sm">Ver Respuestas</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crear encuesta</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" action="app/encuesta.php">
                    <div class="mb-3">
                        <label for="nombre" class="form-label">Nombre Encuesta:</label>
                        <input type="text" class="form-control" id="nombre" name="nombre">
                    </div>  
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
</div>

<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/scripts.js"></script>

</html>